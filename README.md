# Personal ESLint TypeScript Config

My personal ESLint config used for TypeScript based on Airbnb's config.

## Installation

Add package repository

```bash
echo @siphomateke:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
```

Install package

```bash
npx install-peerdeps --dev @siphomateke/eslint-config-typescript
```

## Usage

```js
// eslintrc.js
module.exports = {
  env: {
    node: true,
  },
  parserOptions: {
    project: './tsconfig.json',
    tsconfigRootDir: __dirname,
  },
  extends: [
    '@siphomateke/eslint-config-typescript'
  ],
};
```

> Warning: The parserOptions configuration is required for this config to work properly
